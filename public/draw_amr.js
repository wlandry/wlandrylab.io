function draw_amr(e)
{
    if(click_visible)
    {
        clear_amr();
    }
    var canvas=document.getElementById("AMR");
    if(!canvas.getContext){return;}
    var ctx=canvas.getContext("2d");

    if (!e)
    {
        var e = window.event;
    }
    var x=e.pageX - canvas.offsetLeft;
    var y=e.pageY - canvas.offsetTop;

    ctx.strokeStyle="#000000";
    ctx.strokeRect(0,0,512,512);
    var num_levels=6;
    var element_size=256;
    var startx=0;
    var starty=0;
    var level_color=new Array();
    level_color[0]="#000000";
    level_color[1]="#0000ff";
    level_color[2]="#00ff00";
    level_color[3]="#ff0000";
    level_color[4]="#ff00ff";
    level_color[5]="#ffff00";
    level_color[6]="#00ffff";

    for(l=0;l<num_levels;++l)
    {
        if(x>startx+element_size*2)
            startx+=2*element_size;
        if(y>starty+element_size*2)
            starty+=2*element_size;

        ctx.strokeStyle=level_color[l];
        ctx.beginPath();
        for(xx=-2;xx<3;xx+=2)
        {
            for(yy=-2;yy<3;yy+=2)
            {    
                ctx.moveTo(startx+(xx+1)*element_size,starty+yy*element_size);
                ctx.lineTo(startx+(xx+1)*element_size,
                           starty+(yy+2)*element_size);
                ctx.moveTo(startx+xx*element_size,starty+(yy+1)*element_size);
                ctx.lineTo(startx+(xx+2)*element_size,
                           starty+(yy+1)*element_size);
            }
        }
        ctx.closePath();
        ctx.stroke();

        element_size=element_size/2;
    }
}

function clear_amr()
{
    var canvas=document.getElementById("AMR");
    if(!canvas.getContext){return;}
    var ctx=canvas.getContext("2d");
    ctx.clearRect(0,0,512,512);
    ctx.strokeStyle="#000000";
    ctx.strokeRect(0,0,512,512);
    click_visible=false;
}

function reset_amr()
{
    clear_amr();
    var canvas=document.getElementById("AMR");
    if(!canvas.getContext){return;}
    var ctx=canvas.getContext("2d");
    ctx.font="100px Impact";
    ctx.fillText("Click Me",20,100); 
    click_visible=true;
}

var click_visible=true;

window.onload=reset_amr;
