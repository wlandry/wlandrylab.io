<!DOCTYPE HTML>
<html lang="en">
  <head>
    <title>FTensor</title>
    <link href="../../css/style.css" rel="stylesheet" type="text/css" />
    <meta charset="utf-8" />
    <link rel="stylesheet" href="../../mathscribe/jqmath-0.4.3.css" />
    <script src="../../mathscribe/jquery-1.4.3.min.js"></script>
    <script src="../../mathscribe/jqmath-etc-0.4.6.min.js"></script>
    <style>
      mtd, .fm-mtd  {
      text-align:left;
      }
      img.hundred {
          width: 100%;
      }
    </style>
  </head>

  <body>
    <div id="wrapper">
      <div id="header-wrapper">
        <div id="header">
	  <div id="logo">
	    <h1><a href="../..">Walter Landry</a></h1>
	  </div>
	  <div id="menu">
	    <ul>
	      <li><a href="../..">Home</a></li>
	      <li class="current_page_item"><a href="../../Projects">Projects</a></li>
              <li><a href="../../Presentations">Presentations</a></li>
	      <li><a href="../../Contact">Contact</a></li>
	    </ul>
	  </div>
        </div>
      </div>

      <div id="page">
        <div id="page-bgtop">
	  <div id="page-bgbtm">
	    <div id="content">
	      <div class="post">
	        <h2 class="title"><a>FTensor</a></h2>
	        <div class="entry">
                  <p>FTensor is a C++ library that simplifies the
                  construction of codes that use tensors.</p>
                  <h3>Motivation</h3>
                  <p>As a simple
                  example, consider multiplying a vector $P$ by a
                  matrix $T$ and putting the results into $Q$.</p>

                  $$(\table Q_x; Q_y) =  (\table T_{xx}, T_{xy}; T_{yx}, T_{yy}) (\table P_x; P_y)$$

                  <p>Alternately, you can write it as</p>

                  $$\table Q_x= ∑↙{j=x,y} T_{xj} P_j;
                  Q_y= ∑↙{j=x,y}  T_{yj} P_j$$

                  <p>or even more simply as</p>

                  $$Q_i= ∑↙{j=x,y} T_{ij} P_j$$

                  <p>where the index $i$ is understood to stand for $x$ and
                  $y$ in turn.</p>

                  <p>Einstein introduced the convention that if an index
                  appears in two tensors that multiply each other,
                  then that index is implicitly summed. This mostly
                  removes the need to write the summation symbol ∑.
                  Using this Einstein summation notation,
                  the matrix-vector multiplication becomes simply</p>

                  $$Q_i = T_{ij} P_j$$

                  <p>Of course, now that the notation has become so
                  nice and compact, it becomes easy to write much more
                  complicated formulas such as the definition of the
                  Riemann tensor</p>

                  $$R^{i}_{jkl} = dG^{i}_{jkl} − dG^{i}_{lkj} + G^{m}_{jk} G^{i}_{ml} −
                    G^{m}_{lk} G^{i}_{jm}$$


                  <p>To express this in code with regular C arrays, we
                  could use multidimensional arrays and start writing
                  loops</p>

<pre>
for(int i=0;i&lt;3;++i)
  for(int j=0;j&lt;3;++j)
    for(int k=0;k&lt;3;++k)
      for(int l=0;l&lt;3;++l)
      {
        R[i][j][k][l]=
          dG[i][j][k][l] - dG[i][l][k][j];
                         
        for(int m=0;m&lt;3;++m)
          R[i][j][k][l]+=G[m][j][k]*G[i][m][l]
                         - G[m][l][k]*G[i][m][j];
      }
</pre>
                  
                  <p>This is a dull, mechanical, error-prone task,
                  exactly the type of task that we want computers to
                  do for us.  This style of programming is often
                  referred to as C-tran, since it is programming in
                  C++ but with all of the limitations of Fortran 77.
                  Moreover, the Riemann tensor has a number of
                  symmetries which reduces the total number of
                  independent components from 81 to 6.  So this simple
                  loop does 10 times more work than needed.</p>

                  <p>What we would like is to write this as</p>
<pre>
R(i,j,k,l)=dG(i,j,k,l) - dG(i,l,k,j)
           + G(m,j,k)*G(i,m,l) - G(m,l,k)*G(i,m,j);
</pre>
  <p>and have the computer do all of the summing automatically.</p>

                  <p>There are a number of libraries with varying
                  degrees of support for this notation.  Most of them
                  only provide vectors and matrices, but not higher
                  rank tensors with symmetry properties.  Of those that do
                  provide these higher rank tensors, they produce
                  slow, inefficient code.  The one exception
                  is <a href="https://code.google.com/p/ltensor/">LTensor</a>
                  which was directly inspired by FTensor.</p>

                  <h3 id="Benchmarks">Benchmarks</h3>
                  <p>FTensor achieves high performance by using
                  <a href="https://en.wikipedia.org/wiki/Expression_templates">expression
                  templates</a>.  If you are interested in all of the
                  gory details, you can read
                  the <a href="../../Presentations/FTensor.pdf">paper</a>.  In essence,
                  expression templates allows the compiler to unroll
                  the expressions at compile-time ...  in theory.  In
                  practice, the compiler may not be able to simplify
                  the expressions, and performance can get much
                  worse.</p>

                  <p>For example, consider the infinite sum</p>

                  $$\table y_i = y_i , + a_i ∑↙{n=0}↖\∞ {0.1}^n ; , + 2·b_i ∑↙{n=0}↖\∞ ({0.1·0.2})^n ; , + 3(a_j b_j) c_i ∑↙{n=0}↖\∞ ({0.1·0.2·0.3})^n ; , + 4(a_j c_j)(b_k b_k) d_i ∑↙{n=0}↖\∞ ({0.1·0.2·0.3·0.4})^n… ;$$

                  <p>where we can keep adding terms on the right to
                  make the expression more complicated.  We implement this using FTensor</p>
<pre>
Tensor1&lt;double,3&gt; y(0,1,2);
Tensor1&lt;double,3&gt; a(2,3,4);
Tensor1&lt;double,3&gt; b(5,6,7);
Tensor1&lt;double,3&gt; c(8,9,10);
Tensor1&lt;double,3&gt; d(11,12,13);
...
const Index&lt;'i',3&gt; i;
const Index&lt;'j',3&gt; j;
const Index&lt;'k',3&gt; k;
...


for(int n=0;n&lt;10000000;++n)
  {
    y(i)+=a(i)
      + 2*b(i)
      + 3*(a(j)*b(j))*c(i)
      + 4*(a(j)*c(j))*(b(j)*b(j))*d(i)
      ...
      ;
    a(i)*=0.1;
    b(i)*=0.2;
    c(i)*=0.3;
    d(i)*=0.4;
    ...
  }
</pre>
<p>and compare it to an implementation with a C-tran version.  In
2003, I ran the benchmarks with a number of different compilers on
different chips and operating systems (click on the image for a larger
version).</p>

<a href="Ctran_FTensor_2003.png"><img src="Ctran_FTensor_2003.png" class="hundred" alt="Ctran FTensor 2003"></a>

<p>This shows the relative speed of the C-tran and FTensor versions
versus the number of operators (+ or *) in the expression.  Any points
less than one means that the FTensor version is slower than the C-tran
version.  As you can see, all of the compilers struggled to optimize
the FTensor code.  For all of the details on compiler versions and
options, see the <a href="../../Presentations/FTensor.pdf">paper</a>.</p>

<p>Moving forward a decade, we run the same benchmarks with
new <a href="compilers_2012.html">compilers</a> and see what has
changed.</p>

<a href="Ctran_FTensor_2012.png"><img src="Ctran_FTensor_2012.png" class="hundred" alt="Ctran FTensor 2012"></a>

<p>This plot shows a few interesting features.</p>
<ol>
<li>The FTensor code generated by the Intel and PGI compilers
is generally slower than their C-tran code.  This is similar to the
performance from 2003.</li>
<li>In contrast, the FTensor code generated by the GCC and Clang
compilers is generally faster than their C-tran code.  It is more
efficient to use FTensor, with all of its abstractions, than it is to
write the loops yourself.</li>
</ol> 
<p>So if you are committed to using one of these compilers, then it
becomes more clear whether using FTensor will be a performance burden.
However, that burden may arise because the compiler is really, really
good at optimizing C-tran, but just as good as any other compiler at
optimizing FTensor.  So let's look at the speed of the C-tran versions
normalized by the speed of the Intel C-tran.</p>

<a href="Ctran_Intel_2012.png"><img src="Ctran_Intel_2012.png" class="hundred" alt="Ctran Intel 2012"></a>

<p>For the most complicated expression, Clang and MSVC are 20 times
slower than Intel and PGI.  Open64 and Pathscale's ENZO are slower,
but not by such a huge fraction.  Intel, GCC and PGI are reasonably
competitive with each other.  Now lets look at the relative speed of
the FTensor versions.</p>

<a href="FTensor_Intel_2012.png"><img src="FTensor_Intel_2012.png" class="hundred" alt="FTensor Intel 2012"></a>

<p>There is a clear winner here.  If you want the fastest FTensor
performance, use GCC.</p>

<h3 id="PGO">Profile Guided Optimization</h3>
<p>The results above used standard optimization switches.  It is also
possible to
use <a href="https://en.wikipedia.org/wiki/Profile-guided_optimization">profile
guided optimization</a> (PGO) to further improve performance.  PGO can
be a little tricky to use.  The Open64 compiler produced training
executables that were so slow (at least 100 times slower) that PGO was
impractical.  Intel's training executables were "only" 20 times
slower.  GCC's implementation was the most practical, with executables
that were about 2 times slower.  For the others, it is not clear
whether Clang has implemented PGO, ENZO's PGO was not working in the
version I had, the free Microsoft compiler does not ship with PGO, and
my PGI license expired before I could test PGO.</p>

<p>Comparing the performance of the PGO versus non-PGO results, the
results are mixed.  The Intel C-tran performance is always worse, but
the Intel FTensor performance is always better.  In fact, it is better
than the Intel C-tran PGO performance and became comparable to the
Intel C-tran non-PGO performance.</p>
<p>GCC, on the other hand, only shows an improvement with the most
complicated C-tran expression, and the GCC FTensor performance is
essentially unchanged.</p>

<a href="Ctran_PGO_2012.png"><img src="Ctran_PGO_2012.png" class="hundred" alt="Ctran PGO 2012"></a>
<a href="FTensor_PGO_2012.png"><img src="FTensor_PGO_2012.png" class="hundred" alt="FTensor PGO 2012"></a>

<h3 id="EndNote">End Note</h3>
<p>If you want to reproduce the results here, download the code from the <a href="https://code.google.com/p/ftensor/source/checkout">Mercurial repository</a> and
read the instructions in <code>tests/README</code>.  For the curious,
you can also look at the <a href="compilers_2012.html">compiler
options</a>.</p>
<p>There has been a little discussion of these benchmarks on
the <a href="https://gcc.gnu.org/ml/gcc/2012-06/msg00220.html">GCC</a>,
<a href="https://lists.llvm.org/pipermail/cfe-dev/2012-June/022145.html">Clang</a>, and <a href="https://sourceforge.net/mailarchive/forum.php?thread_name=20120620.093621.1021370527834485186.wlandry%40caltech.edu&forum_name=open64-devel">Open64</a>
mailing lists</p>
	        </div>
	      </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end page -->

      <div id="sidebar">
        <h2>Links</h2>
        <ul>
          <li>
            <ul>
              <li>
                <a href="https://gitlab.com/wlandry/ftensor">FTensor Repository</a>
              </li>
              <li>
                <a href="../../Presentations/FTensor.pdf">Implementing a High Performance Tensor Library</a>
              </li>
              <li>
                <a href="compilers_2012.html">2012 Benchmark compiler options </a>
              </li>
              <li>
                <a href="https://gcc.gnu.org/ml/gcc/2012-06/msg00220.html">GCC Discussion</a>
              </li>
              <li>
                <a href="https://lists.llvm.org/pipermail/cfe-dev/2012-June/022145.html">Clang Discussion</a>
              </li>
              <li>
                <a href="https://sourceforge.net/mailarchive/forum.php?thread_name=20120620.093621.1021370527834485186.wlandry%40caltech.edu&forum_name=open64-devel">Open64 Discussion</a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
      <div style="clear: both;">&nbsp;</div>
      <!-- end #sidebar -->

      <div id="footer">
        <p>Design by <a href="https://web.archive.org/web/20120115100417/http://www.freecsstemplates.org/">Free CSS Templates</a>.</p>
      </div>

    </div>
  </body>
</html>
